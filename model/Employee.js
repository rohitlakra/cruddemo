
const mysqlConnection=require('../config/sqlConnection');

var Employee = function(employee){
    this.EmpID     = employee.EmpID;
    this.Name      = employee.Name;
    this.EmpCode   = employee.EmpCode;
    this.Email     = employee.Email;
    this.Salary    = employee.Salary;
    this.City     = employee.City;
    
  };
  Employee.create = function (newEmp, result) {

    mysqlConnection.query("INSERT INTO Employee set ?", newEmp, function (err, res) {
    if(err) {
      console.log("error: ", err.message);
      result(err, null);
    }
    else{
      console.log(res.insertId);
      result(null, res.insertId);
    }
    });
    };
    
  module.exports=Employee;