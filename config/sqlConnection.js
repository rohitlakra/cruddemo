const mysql=require('mysql');

var mysqlConnection=mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'root',
    database:'EmployeeDB'
    });

    mysqlConnection.connect((err)=>{
        if(!err)
        console.log("DB Connection established.");
        else
        console.log("DB Connection Failed \n Error : "+JSON.stringify(err,undefined,2));
    });

    module.exports=mysqlConnection;