const express=require('express');

const router=express.Router();

const employeeController=require('../controller/employeeController');

router.get('/getEmployee',employeeController.getEmployees);
router.get('/employee/:id',employeeController.getEmployeeByID);
router.post('/createEmployee',employeeController.createEmployee);
router.delete('/deleteEmployee/:id',employeeController.deleteEmployeeByID);
router.put('/updateEmployee/:id',employeeController.updateEmployee);
// router.delete('/deleteUser/:name',UserController.deleteUser);
module.exports=router;