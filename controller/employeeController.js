const Employee = require('../model/Employee');
const mysqlConnection = require('../config/sqlConnection');

/*<-------------------------All EMployees ------------------------------>*/

const getEmployees = function (req, res) {
    mysqlConnection.query('SELECT * FROM Employee', (err, rows, fields) => {
        if (!err) {
            console.log(rows);
            res.send(rows);
        } else {
            res.send(err);
        }
    });
}
/*<-------------------------Get By ID ------------------------------>*/
const getEmployeeByID = function (req, res) {
    mysqlConnection.query('SELECT * FROM Employee WHERE EmpID = ?', [req.params.id], (err, rows, fields) => {
        if (!err) {
            console.log(rows);
            res.send(rows);
        } else {
            res.send(err);
        }
    });

}

/*<-------------------------Delete Employee  ------------------------------>*/
const deleteEmployeeByID = function (req, res) {
    console.log(req.params.id);
    mysqlConnection.query('DELETE FROM Employee WHERE EmpID = ?', [req.params.id], (err, rows, fields) => {
        if (!err) {
            console.log("Deleted Successfully :---",rows);
            res.send(rows);
        } else {
            res.send(err);
        }
    });
}
/*<-------------------------Create Employee  ------------------------------>*/

const createEmployee = function (req, res) {
    const new_employee = new Employee(req.body);
    console.log("what is constructor ;---")
    console.log(req.body.constructor);
    //handles null error
    if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
        res.status(400).send({ error: true, message: 'Please provide all required field' });
    } else {
        Employee.create(new_employee, function (err, employee) {
           // console.log(employee);
            if (err){
                res.send(err.message);}else{
            res.json({ error: false, message: "Employee added successfully!", data: employee });
                }
        });
    }
}
/*<-------------------------Update Employee  ------------------------------>*/

const updateEmployee=function(req,res){

    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({ error:true, message: 'Please provide all required field' });
      }else{
          let emp=new Employee(req.body);
          console.log(emp);
          let id=req.params.id;
          console.log(id);
        mysqlConnection.query("UPDATE Employee SET Name=?,EmpCode=?,Email=?,Salary=?,City=? WHERE EmpID = ?", [emp.Name,emp.EmpCode,emp.Email,emp.Salary,emp.City,id], function (err, emp) {
            if(err) {
              console.log("error: ", err);
              res.send(null,err)
            }else{
                res.json({ error: false, message: "Employee added successfully!", data: emp });
            }
            });
}
}
//-------------------


module.exports = {
    getEmployees,
    createEmployee,
    getEmployeeByID,
    deleteEmployeeByID,
    updateEmployee
}



