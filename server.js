
const express=require('express');
const mysql=require('mysql');
const app=express();
const bodyparser=require('body-parser');
const routes=require("./routers/employeeApi");
app.use(bodyparser.json());

//DbConnection
const mysqlConnection=require("./config/sqlConnection");

//ROutes
app.use('/api',routes);

    //Connect to server
const port=process.env.PORT||3000;
app.listen(port,()=>{
    console.log(`Running on port :---${port}`);
    });


